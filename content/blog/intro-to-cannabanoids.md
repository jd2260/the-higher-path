+++
title = "Introduction to Cannabanoids"
date = "2021-01-17T13:50:46+02:00"
tags = ["science"]
categories = ["science"]
description = "This blog post will break down the various cannabanoids and their effects."
banner = "/img/cannabanoid-graphic.jpg"
+++

What the hell is a cannabanoid? Why do people keep saying these weird acronyms and what do they mean? What is doing _what_ when I use cannabis products? Let's talk about THC & CBD and the effects they have on your body.
