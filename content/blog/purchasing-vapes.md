+++
title = "Buying the Perfect Vape"
date = "2020-12-14T13:50:46+02:00"
tags = ["introduction", "purchasing"]
categories = ["Vaping"]
description = "Interested in purchasing a vape? Have no idea where to start? Dave \"The Vape Guru\" will help you get situated."
banner = "/img/vaping.jpg"
+++

Interested in purchasing a vape? Have no idea where to start? Dave \"The Vape Guru\" will help you get situated.
