+++
title = "Castlegar"
banner = "/img/carousel/castlegar.png"
phone = "(778) 460-0327"
type = "location"
address = "2032 Columbia Ave #102, Castlegar, BC"
summary = "|                    | Open         | Close |\n|:------------------ |:-------------|:----- |\n| Monday - Saturday  | 10am          | 8pm   |\n| Sunday             | 10am         | 4pm   |"
+++

{{<figure src=/img/carousel/castlegar.png >}}