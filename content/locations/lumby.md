+++
title = "Lumby"
banner = "/img/carousel/lumby.png"
address = "1998 Vernon St, Lumby, BC"
phone = "(250) 547-6226"
type = "location"
summary = "|                    | Open         | Close |\n|:------------------ |:-------------|:----- |\n| Monday - Saturday  | 9am          | 8pm   |\n| Sunday             | 10am         | 6pm   |"
+++

{{<figure src=/img/carousel/lumby.png >}}