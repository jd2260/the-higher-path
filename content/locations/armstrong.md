+++
title = "Armstrong"
banner = "/img/carousel/armstrong.png"
address = "2580 Pleasant Valley Blvd, Armstrong, BC"
phone = "(250) 546-9868"
type = "location"
summary = "|                    | Open         | Close |\n|:------------------ |:-------------|:----- |\n| Monday - Saturday  | 9am          | 8pm   |\n| Sunday             | 10am         | 6pm   |"
+++

{{<figure src=/img/carousel/armstrong.png >}}
