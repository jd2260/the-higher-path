+++
title = "Oliver"
banner = "/img/carousel/oliver.png"
type = "location"
address = "5859 Main St, Oliver, BC"
phone = "(250) 498-4135"
summary = "|                    | Open         | Close |\n|:------------------ |:-------------|:----- |\n| Monday - Saturday  | 9am          | 8pm   |\n| Sunday             | 10am         | 8pm   |"
+++

{{<figure src=/img/carousel/oliver.png >}}