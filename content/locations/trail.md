+++
title = "Trail"
type = "location"
banner = "/img/carousel/trail.png"
address = "1320 Cedar Ave, Trail, BC"
phone = "(250) 368-8330"
summary = "|                    | Open         | Close |\n|:------------------ |:-------------|:----- |\n| Monday - Saturday  | 9am          | 8pm   |\n| Sunday             | 10am         | 6pm   |"
+++

{{<figure src=/img/carousel/trail.png >}}